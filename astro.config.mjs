import { defineConfig } from "astro/config";

export default defineConfig({
  site: "https://canrylog.gitlab.io",
  base: "/",
  outDir: "public",
  publicDir: "static",
});

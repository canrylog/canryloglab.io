dev:
	npx astro dev

check:
	npx astro check

build: check
	npx astro build

preview:
	npx astro preview

format:
	npx prettier -w src
